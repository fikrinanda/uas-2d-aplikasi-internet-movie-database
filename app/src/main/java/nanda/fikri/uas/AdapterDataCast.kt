package nanda.fikri.uas

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterDataCast(val dataCast : List<HashMap<String,String>>,
                      val movieActivity: MovieActivity) :
    RecyclerView.Adapter<AdapterDataCast.HolderDataCast>(){

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataCast.HolderDataCast {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_cast,p0,false)
        return HolderDataCast(v)
    }

    override fun getItemCount(): Int {
        return dataCast.size
    }

    override fun onBindViewHolder(p0: AdapterDataCast.HolderDataCast, p1: Int) {

        val data = dataCast.get(p1)
        p0.txNc.setText(data.get("nama_celeb"))
        p0.txSbg.setText(data.get("nama_cast"))

        p0.cCt.setOnClickListener ( View.OnClickListener {
            val intent = Intent(movieActivity, CelebActivity::class.java)
            intent.putExtra("id_celeb",data.get("id_celeb"))
            intent.putExtra("nama_celeb",data.get("nama_celeb"))
            intent.putExtra("url",data.get("url"))

            movieActivity.startActivity(intent)
        } )

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).fit().centerCrop().into(p0.imageView2);
    }

    inner class HolderDataCast(v : View) : RecyclerView.ViewHolder(v){
        val txNc = v.findViewById<TextView>(R.id.txNc)
        val txSbg = v.findViewById<TextView>(R.id.txSbg)
        val imageView2 = v.findViewById<ImageView>(R.id.imageView2)
        val cCt = v.findViewById<ConstraintLayout>(R.id.cCt)
    }
}