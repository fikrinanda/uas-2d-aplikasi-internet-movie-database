package nanda.fikri.uas

import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterDataMovieGenre(val dataMovie2 : List<HashMap<String,String>>,
                            val movieGenreActivity: MovieGenreActivity) :
    RecyclerView.Adapter<AdapterDataMovieGenre.HolderDataMovieGenre>(){
    override fun onCreateViewHolder(
        p0: ViewGroup,
        p1: Int
    ): AdapterDataMovieGenre.HolderDataMovieGenre {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_movie,p0,false)
        return HolderDataMovieGenre(v)
    }

    override fun getItemCount(): Int {
        return dataMovie2.size
    }

    override fun onBindViewHolder(
       p0: AdapterDataMovieGenre.HolderDataMovieGenre,
        p1: Int
    ) {
        val data = dataMovie2.get(p1)
        p0.txJudul.setText(data.get("nama_movie"))

        if(p1.rem(2) == 0) p0.cMovie.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cMovie.setBackgroundColor(Color.rgb(255,255,245))

        p0.cMovie.setOnClickListener ( View.OnClickListener {

            val intent = Intent(movieGenreActivity, MovieActivity::class.java)
            intent.putExtra("id_movie",data.get("id_movie"))
            intent.putExtra("nama_movie",data.get("nama_movie"))
            intent.putExtra("tahun_movie",data.get("tahun_movie"))
            intent.putExtra("sinopsis_movie",data.get("sinopsis_movie"))
            intent.putExtra("nama_genre",data.get("nama_genre"))
            intent.putExtra("url",data.get("url"))
            intent.putExtra("url2",data.get("url2"))

            movieGenreActivity.startActivity(intent)
        } )

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).fit().centerCrop().into(p0.photo);
    }

    inner class HolderDataMovieGenre(v : View) : RecyclerView.ViewHolder(v){
        val txJudul = v.findViewById<TextView>(R.id.txJudul)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val cMovie = v.findViewById<ConstraintLayout>(R.id.cMovie)
    }
}