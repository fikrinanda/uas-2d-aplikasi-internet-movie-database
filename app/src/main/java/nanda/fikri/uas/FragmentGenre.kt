package nanda.fikri.uas

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.MotionEventCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray

class FragmentGenre : Fragment() {

    lateinit var thisParent : MainActivity
    lateinit var v : View

    lateinit var genreAdapter : AdapterDataGenre
    val daftarGenre = mutableListOf<HashMap<String,String>>()
    val url = "http://192.168.43.157/uas_android/show_data_genre.php"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity

        v = inflater.inflate(R.layout.frag_data_genre,container,false)

        genreAdapter = AdapterDataGenre(daftarGenre, this)
        var listGenre = v.findViewById<RecyclerView>(R.id.listGenre)
        listGenre.layoutManager = LinearLayoutManager(thisParent)
        listGenre.adapter = genreAdapter

        return v
    }

    override fun onStart() {
        super.onStart()
        showDataGenre()
    }

    fun showDataGenre(){
        val request = StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarGenre.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var genre = HashMap<String,String>()
                    genre.put("nama_genre",jsonObject.getString("nama_genre"))
                    genre.put("url",jsonObject.getString("url"))
                    daftarGenre.add(genre)
                }
                genreAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            })
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }
}