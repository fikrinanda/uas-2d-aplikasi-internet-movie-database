package nanda.fikri.uas

import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterDataGenre(val dataGenre : List<HashMap<String,String>>,
                       val fragmentGenre: FragmentGenre) :
    RecyclerView.Adapter<AdapterDataGenre.HolderDataGenre>(){

    override fun onCreateViewHolder(
        p0: ViewGroup,
        p1: Int
    ): AdapterDataGenre.HolderDataGenre {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_genre,p0,false)
        return HolderDataGenre(v)
    }

    override fun getItemCount(): Int {
        return dataGenre.size
    }

    override fun onBindViewHolder(p0: AdapterDataGenre.HolderDataGenre, p1: Int) {
        val data = dataGenre.get(p1)
        p0.txGenre.setText(data.get("nama_genre"))

        if(p1.rem(2) == 0) p0.cGenre.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cGenre.setBackgroundColor(Color.rgb(255,255,245))

        p0.cGenre.setOnClickListener(View.OnClickListener {
            val intent = Intent(fragmentGenre.thisParent, MovieGenreActivity::class.java)
            intent.putExtra("nama_genre",data.get("nama_genre"))

            fragmentGenre.startActivity(intent)
        })



        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.bg);
    }

    inner class HolderDataGenre(v : View) : RecyclerView.ViewHolder(v){
        val txGenre = v.findViewById<TextView>(R.id.txGenre)
        val bg = v.findViewById<ImageView>(R.id.imGenre)
        val cGenre = v.findViewById<ConstraintLayout>(R.id.cGenre)
    }
}