package nanda.fikri.uas

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray

class FragmentCeleb : Fragment() {

    lateinit var thisParent : MainActivity
    lateinit var v : View

    lateinit var celebAdapter : AdapterDataCeleb
    var daftarCeleb = mutableListOf<HashMap<String,String>>()
    val url = "http://192.168.43.157/uas_android/show_data_celeb.php"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity

        v = inflater.inflate(R.layout.frag_data_celeb,container,false)

        celebAdapter = AdapterDataCeleb(daftarCeleb, this)
        var listCeleb = v.findViewById<RecyclerView>(R.id.listCeleb)
        listCeleb.layoutManager = LinearLayoutManager(thisParent)
        listCeleb.adapter = celebAdapter

        return v
    }

    override fun onStart() {
        super.onStart()
        showDataCeleb()
    }

    fun showDataCeleb(){
        val request = StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarCeleb.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var celeb = HashMap<String,String>()
                    celeb.put("id_celeb",jsonObject.getString("id_celeb"))
                    celeb.put("nama_celeb",jsonObject.getString("nama_celeb"))
                    celeb.put("url",jsonObject.getString("url"))
                    daftarCeleb.add(celeb)
                }
                celebAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            })
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }
}