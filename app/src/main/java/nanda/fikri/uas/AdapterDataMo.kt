package nanda.fikri.uas

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_mo.*

class AdapterDataMo(val dataMo : List<HashMap<String,String>>,
                    val moActivity: MoActivity) :
    RecyclerView.Adapter<AdapterDataMo.HolderDataMo>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataMo.HolderDataMo {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_mo,p0,false)
        return HolderDataMo(v)
    }

    override fun getItemCount(): Int {
        return dataMo.size
    }

    override fun onBindViewHolder(p0: AdapterDataMo.HolderDataMo, p1: Int) {
        val data = dataMo.get(p1)
        p0.txID.setText(data.get("id_movie"))
        p0.txJdl.setText(data.get("nama_movie"))

        p0.cMo.setOnClickListener(View.OnClickListener {
            moActivity.txNaMo.setText(data.get("nama_movie"))
            moActivity.id_movie = data.get("id_movie").toString()
            moActivity.tahun_movie = data.get("tahun_movie").toString()
            moActivity.sinopsis_movie = data.get("sinopsis_movie").toString()
            moActivity.nama_genre = data.get("nama_genre").toString()
            moActivity.cover_movie = data.get("url").toString()


        })
    }

    inner class HolderDataMo(v : View) : RecyclerView.ViewHolder(v){
        val txID = v.findViewById<TextView>(R.id.txID)
        val txJdl = v.findViewById<TextView>(R.id.txJdl)
        val cMo = v.findViewById<ConstraintLayout>(R.id.cMo)
    }
}