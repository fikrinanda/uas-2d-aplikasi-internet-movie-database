package nanda.fikri.uas

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_mo.*

class AdapterDataCe (val dataMo : List<HashMap<String,String>>,
                     val ceActivity: CeActivity) :
    RecyclerView.Adapter<AdapterDataCe.HolderDataCe>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataCe.HolderDataCe {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_mo, p0, false)
        return HolderDataCe(v)
    }

    override fun getItemCount(): Int {
        return dataMo.size
    }

    override fun onBindViewHolder(p0: AdapterDataCe.HolderDataCe, p1: Int) {
        val data = dataMo.get(p1)
        p0.txID.setText(data.get("id_celeb"))
        p0.txJdl.setText(data.get("nama_celeb"))

        p0.cMo.setOnClickListener(View.OnClickListener {
            ceActivity.txNaMo.setText(data.get("nama_celeb"))
            ceActivity.id_celeb = data.get("id_celeb").toString()
            ceActivity.foto_celeb = data.get("url").toString()

        })
    }

    inner class HolderDataCe(v: View) : RecyclerView.ViewHolder(v) {
        val txID = v.findViewById<TextView>(R.id.txID)
        val txJdl = v.findViewById<TextView>(R.id.txJdl)
        val cMo = v.findViewById<ConstraintLayout>(R.id.cMo)
    }
}