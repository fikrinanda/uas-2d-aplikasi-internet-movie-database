package nanda.fikri.uas

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import android.widget.ScrollView
import android.widget.Toast
import android.widget.VideoView
import androidx.core.view.MotionEventCompat
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.frag_data_movie.*
import kotlinx.android.synthetic.main.row_movie.*
import org.json.JSONArray


class FragmentMovie : Fragment() {

    lateinit var thisParent : MainActivity
    lateinit var v : View

    lateinit var movieAdapter : AdapterDataMovie
    var daftarMovie = mutableListOf<HashMap<String,String>>()
    val url = "http://192.168.43.157/uas_android/show_data_movie.php"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity

        v = inflater.inflate(R.layout.frag_data_movie,container,false)

        movieAdapter = AdapterDataMovie(daftarMovie, this)
        var listMovie = v.findViewById<RecyclerView>(R.id.listMovie)
        listMovie.layoutManager = LinearLayoutManager(thisParent)
        listMovie.adapter = movieAdapter

        return v
    }

    override fun onStart() {
        super.onStart()
        showDataMovie()
    }


    fun showDataMovie(){
        val request = StringRequest(Request.Method.POST,url,
            Response.Listener { response ->
                daftarMovie.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var movie = HashMap<String,String>()
                    movie.put("id_movie",jsonObject.getString("id_movie"))
                    movie.put("nama_movie",jsonObject.getString("nama_movie"))
                    movie.put("tahun_movie",jsonObject.getString("tahun_movie"))
                    movie.put("sinopsis_movie",jsonObject.getString("sinopsis_movie"))
                    movie.put("nama_genre",jsonObject.getString("nama_genre"))
                    movie.put("url",jsonObject.getString("url"))
                    movie.put("url2",jsonObject.getString("url2"))
                    daftarMovie.add(movie)
                }
                movieAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent, "Terjadi kesalahan koneksi ke server",Toast.LENGTH_SHORT).show()
            })
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }



}