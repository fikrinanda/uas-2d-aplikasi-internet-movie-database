package nanda.fikri.uas

import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterDataCeleb(val dataCeleb : List<HashMap<String,String>>,
                       val fragmentCeleb: FragmentCeleb) :
    RecyclerView.Adapter<AdapterDataCeleb.HolderDataCeleb>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataCeleb.HolderDataCeleb {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_celeb,p0,false)
        return HolderDataCeleb(v)
    }

    override fun getItemCount(): Int {
        return dataCeleb.size
    }

    override fun onBindViewHolder(p0: AdapterDataCeleb.HolderDataCeleb, p1: Int) {
        val data = dataCeleb.get(p1)
        p0.txNama.setText(data.get("nama_celeb"))

        if(p1.rem(2) == 0) p0.cCeleb.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cCeleb.setBackgroundColor(Color.rgb(255,255,245))

        p0.cCeleb.setOnClickListener ( View.OnClickListener {

            val intent = Intent(fragmentCeleb.thisParent, CelebActivity::class.java)
            intent.putExtra("id_celeb",data.get("id_celeb"))
            intent.putExtra("nama_celeb",data.get("nama_celeb"))
            intent.putExtra("url",data.get("url"))

            fragmentCeleb.startActivity(intent)
        } )

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).fit().centerCrop().into(p0.imCeleb);
    }


    inner class HolderDataCeleb(v : View) : RecyclerView.ViewHolder(v){
        val txNama = v.findViewById<TextView>(R.id.txNama)
        val imCeleb = v.findViewById<ImageView>(R.id.imCeleb)
        val cCeleb = v.findViewById<ConstraintLayout>(R.id.cCeleb)
    }
}