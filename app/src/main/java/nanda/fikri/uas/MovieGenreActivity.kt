package nanda.fikri.uas

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_movie_genre.*
import org.json.JSONArray

class MovieGenreActivity : AppCompatActivity() {

    lateinit var v : View

    lateinit var movieAdapter2 : AdapterDataMovieGenre
    val daftarMovie2 = mutableListOf<HashMap<String,String>>()
    val url = "http://192.168.43.157/uas_android/show_data_movie2.php"
    var a : String? = ""
    var b : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_genre)

        movieAdapter2 = AdapterDataMovieGenre(daftarMovie2, this)
        listMovie2.layoutManager = LinearLayoutManager(this)
        listMovie2.adapter = movieAdapter2

        var paket : Bundle? = intent.extras
        a = paket?.getString("nama_genre")
        b = a.toString()
    }

    override fun onStart() {
        super.onStart()
        showDataMovie2(b)
    }

    fun showDataMovie2(namaGenre : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarMovie2.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var movie = HashMap<String,String>()
                    movie.put("id_movie",jsonObject.getString("id_movie"))
                    movie.put("nama_movie",jsonObject.getString("nama_movie"))
                    movie.put("tahun_movie",jsonObject.getString("tahun_movie"))
                    movie.put("sinopsis_movie",jsonObject.getString("sinopsis_movie"))
                    movie.put("nama_genre",jsonObject.getString("nama_genre"))
                    movie.put("url",jsonObject.getString("url"))
                    movie.put("url2",jsonObject.getString("url2"))
                    daftarMovie2.add(movie)
                }
                movieAdapter2.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("nama_genre",namaGenre)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}