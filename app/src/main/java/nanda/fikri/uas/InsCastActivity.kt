package nanda.fikri.uas

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_insert_cast.*
import kotlinx.android.synthetic.main.activity_insert_celeb.*
import kotlinx.android.synthetic.main.activity_insert_movie.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class InsCastActivity : AppCompatActivity() {

    lateinit var movieAdapter : ArrayAdapter<String>
    var daftarMovie = mutableListOf<String>()
    val url = "http://192.168.43.157/uas_android/get_nama_movie.php"
    var pilihMovie = ""

    lateinit var celebAdapter : ArrayAdapter<String>
    var daftarCeleb = mutableListOf<String>()
    val url2 = "http://192.168.43.157/uas_android/get_nama_celeb.php"
    var pilihCeleb = ""

    val url3 = "http://192.168.43.157/uas_android/ins_cast.php"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_insert_cast)

        movieAdapter = ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item,daftarMovie)
        spinMo.adapter = movieAdapter
        spinMo.onItemSelectedListener = itemSelected

        celebAdapter = ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item,daftarCeleb)
        spinCe.adapter = celebAdapter
        spinCe.onItemSelectedListener = itemSelected2

        btnK.setOnClickListener(View.OnClickListener {
            queryInsertUpdateDelete("insert")
        })


    }

    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(p0: AdapterView<*>?) {
            spinMo.setSelection(0)
            pilihMovie = daftarMovie.get(0)
        }

        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
            pilihMovie = daftarMovie.get(p2)
        }
    }

    val itemSelected2 = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(p0: AdapterView<*>?) {
            spinCe.setSelection(0)
            pilihCeleb = daftarCeleb.get(0)
        }

        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
            pilihCeleb = daftarCeleb.get(p2)
        }
    }

    override fun onStart() {
        super.onStart()
        getNamaMovie()
        getNamaCeleb()
    }

    fun getNamaMovie(){
        val request = StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarMovie.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarMovie.add(jsonObject.getString("nama_movie"))
                }
                movieAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->  }
        )

        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getNamaCeleb(){
        val request = StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarCeleb.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarCeleb.add(jsonObject.getString("nama_celeb"))
                }
                celebAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->  }
        )

        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    edNmaCst.setText("")
                    spinMo.setSelection(0)
                    pilihMovie = daftarMovie.get(0)
                    spinCe.setSelection(0)
                    pilihCeleb = daftarCeleb.get(0)
                }
                else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(
                    Date()
                )+".jpg"
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("nama_movie",pilihMovie)
                        hm.put("nama_celeb",pilihCeleb)
                        hm.put("nama_cast",edNmaCst.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}