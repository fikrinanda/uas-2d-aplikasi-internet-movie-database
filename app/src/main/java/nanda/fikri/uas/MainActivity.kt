package nanda.fikri.uas

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var fragMovie : FragmentMovie
    lateinit var fragGenre : FragmentGenre
    lateinit var fragCeleb : FragmentCeleb

    lateinit var ft : FragmentTransaction

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.itemHome -> FrameLayout.visibility = View.GONE

            R.id.itemMovies ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.FrameLayout,fragMovie).commit()
                FrameLayout.setBackgroundColor(Color.WHITE)
                FrameLayout.visibility = View.VISIBLE
            }

            R.id.itemGenres ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.FrameLayout,fragGenre).commit()
                FrameLayout.setBackgroundColor(Color.WHITE)
                FrameLayout.visibility = View.VISIBLE
            }

            R.id.itemCelebs ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.FrameLayout,fragCeleb).commit()
                FrameLayout.setBackgroundColor(Color.WHITE)
                FrameLayout.visibility = View.VISIBLE
            }

        }
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        topNavigationView.setOnNavigationItemSelectedListener(this)
        fragMovie = FragmentMovie()
        fragGenre = FragmentGenre()
        fragCeleb = FragmentCeleb()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_option,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemMovie ->{
                val intent = Intent(this, MoActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.itemCeleb ->{
                val intent = Intent(this, CeActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.itemSetting ->{
                val intent = Intent(this, InsCastActivity::class.java)
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
