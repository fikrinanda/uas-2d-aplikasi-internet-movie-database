package nanda.fikri.uas

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_celeb.*
import kotlinx.android.synthetic.main.activity_movie.*
import org.json.JSONArray

class CelebActivity : AppCompatActivity() {

    lateinit var movie2Adapter : AdapterDataMovie2
    var daftarMovie2 = mutableListOf<HashMap<String,String>>()
    val url = "http://192.168.43.157/uas_android/show_data_movie3.php"

    var id_celeb : String? = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_celeb)

        movie2Adapter = AdapterDataMovie2(daftarMovie2, this)
        var listMovie2 = findViewById<RecyclerView>(R.id.listMovie2)
        listMovie2.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        listMovie2.adapter = movie2Adapter

        var paket : Bundle? = intent.extras
        id_celeb = paket?.getString("id_celeb")
        txNm.setText(paket?.getString("nama_celeb"))

        Picasso.get().load(paket?.getString("url")).fit().centerCrop().into(imFt)
    }

    override fun onStart() {
        super.onStart()
        showDataMovie2(id_celeb.toString())
    }

    fun showDataMovie2(idCeleb : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarMovie2.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var cast = HashMap<String,String>()
                    cast.put("id_celeb",jsonObject.getString("id_celeb"))
                    cast.put("nama_movie",jsonObject.getString("nama_movie"))
                    cast.put("nama_cast",jsonObject.getString("nama_cast"))
                    cast.put("tahun_movie",jsonObject.getString("tahun_movie"))
                    cast.put("url",jsonObject.getString("url"))
                    cast.put("id_movie",jsonObject.getString("id_movie"))
                    cast.put("sinopsis_movie",jsonObject.getString("sinopsis_movie"))
                    cast.put("nama_genre",jsonObject.getString("nama_genre"))
                    cast.put("url2",jsonObject.getString("url2"))
                    daftarMovie2.add(cast)
                }
                movie2Adapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("id_celeb",idCeleb)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}