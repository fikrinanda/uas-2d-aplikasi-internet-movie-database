package nanda.fikri.uas

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterDataMovie2(val dataMv : List<HashMap<String,String>>,
                        val celebActivity: CelebActivity) :
    RecyclerView.Adapter<AdapterDataMovie2.HolderDataMovie2>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataMovie2.HolderDataMovie2 {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_movie2,p0,false)
        return HolderDataMovie2(v)
    }

    override fun getItemCount(): Int {
        return dataMv.size
    }

    override fun onBindViewHolder(p0: AdapterDataMovie2.HolderDataMovie2, p1: Int) {
        val data = dataMv.get(p1)
        p0.txNma.setText(data.get("nama_movie"))
        p0.txSb.setText(data.get("nama_cast"))
        p0.txTh.setText(data.get("tahun_movie"))

        p0.cM2.setOnClickListener(View.OnClickListener {
            val intent = Intent(celebActivity, MovieActivity::class.java)
            intent.putExtra("id_movie",data.get("id_movie"))
            intent.putExtra("nama_movie",data.get("nama_movie"))
            intent.putExtra("tahun_movie",data.get("tahun_movie"))
            intent.putExtra("sinopsis_movie",data.get("sinopsis_movie"))
            intent.putExtra("nama_genre",data.get("nama_genre"))
            intent.putExtra("url",data.get("url"))
            intent.putExtra("url2",data.get("url2"))

            celebActivity.startActivity(intent)
        })

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).fit().centerCrop().into(p0.imCv);
    }

    inner class HolderDataMovie2(v : View) : RecyclerView.ViewHolder(v){
        val txNma = v.findViewById<TextView>(R.id.txNma)
        val txSb = v.findViewById<TextView>(R.id.txSb)
        val txTh = v.findViewById<TextView>(R.id.txTh)
        val imCv = v.findViewById<ImageView>(R.id.imCv)
        val cM2 = v.findViewById<ConstraintLayout>(R.id.cM2)
    }
}