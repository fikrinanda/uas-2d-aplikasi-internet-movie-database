package nanda.fikri.uas

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_insert_movie.*
import kotlinx.android.synthetic.main.activity_mo.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class MoActivity : AppCompatActivity() {

    lateinit var moAdapter : AdapterDataMo
    val daftarMo = mutableListOf<HashMap<String,String>>()
    val url = "http://192.168.43.157/uas_android/show_data_movie.php"
    val url2 = "http://192.168.43.157/uas_android/ins_upd_del_movie.php"
    var id_movie = ""
    var tahun_movie = ""
    var sinopsis_movie = ""
    var nama_genre = ""
    var cover_movie = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mo)

        moAdapter = AdapterDataMo(daftarMo, this)
        var listMo = findViewById<RecyclerView>(R.id.listMo)
        listMo.layoutManager = LinearLayoutManager(this)
        listMo.adapter = moAdapter

        btnInsert.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, InsMoActivity::class.java)
            startActivity(intent)
        })

        btnUpdate.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, UpdMoActivity::class.java)
            intent.putExtra("id_movie",id_movie)
            intent.putExtra("nama_movie",txNaMo.text.toString())
            intent.putExtra("tahun_movie",tahun_movie)
            intent.putExtra("sinopsis_movie",sinopsis_movie)
            intent.putExtra("nama_genre",nama_genre)
            intent.putExtra("url",cover_movie)
            startActivity(intent)


        })

        btnDelete.setOnClickListener(View.OnClickListener {
            queryInsertUpdateDelete("delete")
        })
    }

    override fun onStart() {
        super.onStart()
        showDataMo()
    }


    fun showDataMo(){
        val request = StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarMo.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mo = HashMap<String,String>()
                    mo.put("id_movie",jsonObject.getString("id_movie"))
                    mo.put("nama_movie",jsonObject.getString("nama_movie"))
                    mo.put("tahun_movie",jsonObject.getString("tahun_movie"))
                    mo.put("sinopsis_movie",jsonObject.getString("sinopsis_movie"))
                    mo.put("nama_genre",jsonObject.getString("nama_genre"))
                    mo.put("url",jsonObject.getString("url"))
                    mo.put("url2",jsonObject.getString("url2"))
                    daftarMo.add(mo)
                }
                moAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataMo()
                }
                else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(
                    Date()
                )+".jpg"
                when(mode){
//                    "insert" ->{
//                        hm.put("mode","insert")
//                        hm.put("nama_movie",edNamaMovie.text.toString())
//                        hm.put("tahun_movie",edTahunMovie.text.toString())
//                        hm.put("sinopsis_movie",edSinopsisMovie.text.toString())
//                        hm.put("image",imStr)
//                        hm.put("file",nmFile)
//                        hm.put("nama_genre",pilihGenre)
//                    }
//                    "update" ->{
//                        hm.put("mode","update")
//                        hm.put("id_movie",id_movie.toString())
//                        hm.put("nama_movie",edNamaMovie.text.toString())
//                        hm.put("tahun_movie",edTahunMovie.text.toString())
//                        hm.put("sinopsis_movie",edSinopsisMovie.text.toString())
//                        hm.put("image",imStr)
//                        hm.put("file",nmFile)
//                        hm.put("nama_genre",pilihGenre)
//                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("id_movie",id_movie.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}