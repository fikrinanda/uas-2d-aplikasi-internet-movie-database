package nanda.fikri.uas

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.MediaController
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_movie.*
import org.json.JSONArray

class MovieActivity : AppCompatActivity() {

    lateinit var mediaController : MediaController
    lateinit var castAdapter : AdapterDataCast
    var daftarCast = mutableListOf<HashMap<String,String>>()
    val url = "http://192.168.43.157/uas_android/show_data_cast.php"

    var id_movie : String? = ""
    var video : String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie)

        castAdapter = AdapterDataCast(daftarCast, this)
        var listCast = findViewById<RecyclerView>(R.id.listCast)
        listCast.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        listCast.adapter = castAdapter

        var paket : Bundle? = intent.extras
        id_movie = paket?.getString("id_movie")
        txJudul.setText(paket?.getString("nama_movie"))
        txTahun.setText(paket?.getString("tahun_movie"))
        txSinopsis.setText(paket?.getString("sinopsis_movie"))
        btnGenre.setText(paket?.getString("nama_genre"))

        Picasso.get().load(paket?.getString("url")).fit().centerCrop().into(imCover)
        video = paket?.getString("url2")

        mediaController = MediaController(this)
        mediaController.setAnchorView(videoView)
        videoView.setMediaController(mediaController)
        videoView.setVideoURI(Uri.parse(video))
        videoView.seekTo(100)

        btnGenre.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, MovieGenreActivity::class.java)
            intent.putExtra("nama_genre",btnGenre.text)

            startActivity(intent)
        })

    }

    override fun onStart() {
        super.onStart()
        showDataCast(id_movie.toString())
    }

    fun showDataCast(idMovie : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarCast.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var cast = HashMap<String,String>()
                    cast.put("id_movie",jsonObject.getString("id_movie"))
                    cast.put("nama_celeb",jsonObject.getString("nama_celeb"))
                    cast.put("nama_cast",jsonObject.getString("nama_cast"))
                    cast.put("url",jsonObject.getString("url"))
                    cast.put("id_celeb",jsonObject.getString("id_celeb"))
                    daftarCast.add(cast)
                }
                castAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("id_movie",idMovie)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


}