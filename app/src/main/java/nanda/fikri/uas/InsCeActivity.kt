package nanda.fikri.uas

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_insert_celeb.*
import kotlinx.android.synthetic.main.activity_insert_movie.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class InsCeActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imageView4 -> {
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent, mediaHelper.getRcGallery())
            }
            R.id.btnSnd -> {
                queryInsertUpdateDelete("insert")
            }
        }
    }

    lateinit var mediaHelper: MediaHelper
    val url2 = "http://192.168.43.157/uas_android/ins_upd_del_celeb.php"
    var imStr = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_insert_celeb)

        mediaHelper = MediaHelper(this)

        imageView4.setOnClickListener(this)
        btnSnd.setOnClickListener(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == mediaHelper.getRcGallery()) {
                imStr = mediaHelper.getBitmapToString(data!!.data, imageView4)
            }
        }
    }



    fun queryInsertUpdateDelete(mode: String) {
        val request = object : StringRequest(
            Method.POST, url2,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if (error.equals("000")) {
                    Toast.makeText(this, "Operasi Berhasil", Toast.LENGTH_LONG).show()
                    imageView4.setImageResource(R.color.colorHolo)
                    edNmC.setText("")
                } else {
                    Toast.makeText(this, "Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                val nmFile = "DC" + SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(
                    Date()
                ) + ".jpg"
                when (mode) {
                    "insert" -> {
                        hm.put("mode", "insert")
                        hm.put("nama_celeb", edNmC.text.toString())
                        hm.put("image", imStr)
                        hm.put("file", nmFile)

                    }
//                    "update" ->{
//                        hm.put("mode","update")
//                        hm.put("nim",edNim.text.toString())
//                        hm.put("nama",edNamaMhs.text.toString())
//                        hm.put("alamat",edAlamat.text.toString())
//                        hm.put("jenis_kelamin",var1)
//                        hm.put("image",imStr)
//                        hm.put("file",nmFile)
//                        hm.put("nama_prodi",pilihProdi)
//                    }
//                    "delete" ->{
//                        hm.put("mode","delete")
//                        hm.put("nim",edNim.text.toString())
//                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}